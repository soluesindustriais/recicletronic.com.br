<? $h1 = "Reciclagem de lixo eletrônico"; $title  = "Reciclagem de lixo eletrônico"; $desc = "Receba uma cotação de Reciclagem de lixo eletrônico, você vai achar nas buscas do Soluções Industriais, receba uma estimativa de valor hoje com dezena"; $key  = "Reciclagem Componentes Eletrônicos, Reciclagem de informática"; include ('inc/head.php')?>

<body>
    <? include ('inc/header.php');?>
    <main><?=$caminhoservicos; include('inc/servicos/servicos-linkagem-interna.php');?><div
            class='container-fluid mb-2'>
            <? include('inc/servicos/servicos-buscas-relacionadas.php');?>
            <div class="container p-0">
                <div class="row no-gutters">
                    <section class="col-md-9 col-sm-12">
                        <div class="card card-body LeiaMais">
                            <h1 class="pb-2"><?=$h1?></h1>
                            <article>
                                
                            <p>Nos &uacute;ltimos anos, as autoridades mundiais t&ecirc;m debatido sobre a import&acirc;ncia da sustentabilidade e as t&eacute;cnicas que podem ser adotadas para diminuir os impactos causados pelo ser humano no meio ambiente. Uma das solu&ccedil;&otilde;es encontradas foi a reciclagem de lixo eletr&ocirc;nico.</p>

                            <p>Mesmo que o servi&ccedil;o ainda n&atilde;o tenha sido expandido ao redor do mundo, prova disso &eacute; o fato de que apenas 20% do total de res&iacute;duos produzidos s&atilde;o reciclados, a pr&aacute;tica est&aacute; em constante desenvolvimento e j&aacute; virou at&eacute; lei no Brasil.</p>

                            <p>Por exemplo, o estado de S&atilde;o Paulo sancionou em julho de 2009 a lei 13.576, que relata que &ldquo;os produtos e os componentes eletroeletr&ocirc;nicos considerados lixo tecnol&oacute;gico devem receber destina&ccedil;&atilde;o final adequada que n&atilde;o provoque danos ou impactos negativos ao meio ambiente e &agrave; sociedade&rdquo;.</p>

                            <p>Mais recentemente, a Pol&iacute;tica Nacional de Res&iacute;duos S&oacute;lidos (Lei n&ordm; 12.305/2010) ganhou um novo termo, envolvendo justamente os eletr&ocirc;nicos. Dessa vez, o decreto obriga as empresas do setor a implantarem sistemas de coleta e destina&ccedil;&atilde;o de res&iacute;duos, incentivando as pr&oacute;prias prefeituras a comprarem equipamentos advindos dessas companhias.&nbsp;</p>

                            <h2>POR QUE RECICLAR O LIXO ELETR&Ocirc;NICO?&nbsp;</h2>

                            <p>Dificilmente algo ir&aacute; virar lei se n&atilde;o tiver um motivo muito maior por tr&aacute;s do projeto. Algumas vezes, a ideia n&atilde;o &eacute; algo muito positivo para a popula&ccedil;&atilde;o, mas esse n&atilde;o &eacute; o caso da reciclagem de lixo eletr&ocirc;nico.&nbsp;</p>

                            <p>Resumidamente, a base dela &eacute; garantir a preserva&ccedil;&atilde;o do meio ambiente, protegendo o solo contra contamina&ccedil;&otilde;es, aliviando os aterros sanit&aacute;rios e conservando a &aacute;gua e a vida marinha. Contudo, ela n&atilde;o pode - e nem deve! - ser limitada apenas a isso!&nbsp;</p>

                            <p>Muitas pessoas n&atilde;o sabem, mas os aparelhos eletr&ocirc;nicos s&atilde;o produzidos com componentes de origem inorg&acirc;nica, como o cobre, o alum&iacute;nio, o merc&uacute;rio, o c&aacute;dmio, o chumbo, etc, alguns com alta toxicidade e que podem causar danos irrepar&aacute;veis para a flora e a fauna.&nbsp;</p>

                            <p>Al&eacute;m disso, a maioria dos metais utilizados para a fabrica&ccedil;&atilde;o dos dispositivos s&atilde;o finitos, fazendo com que a reciclagem de lixo eletr&ocirc;nico seja fundamental para recuperar e reutilizar mat&eacute;rias-primas preciosas e que podem se tornar inexistentes na natureza daqui uns anos.&nbsp;</p>

                            <h2>PROCEDIMENTOS DE RECICLAGEM DE LIXO ELETR&Ocirc;NICO</h2>

                            <p>A lista de objetos, acess&oacute;rios e aparelhos classificados como eletr&ocirc;nicos &eacute; muito longa, visto que inclui eletroeletr&ocirc;nicos, eletrodom&eacute;sticos e equipamentos industriais. Dentre as principais itens que podem ser citados, destacam-se:&nbsp;</p>

                            <ul>
                                <li>Fios e pilhas;&nbsp;</li>
                                <li>Geladeiras e fog&otilde;es;&nbsp;</li>
                                <li>Smartphones e tablets;&nbsp;</li>
                                <li>Computadores e televisores;&nbsp;</li>
                                <li>C&acirc;meras fotogr&aacute;ficas e impressoras;&nbsp;</li>
                                <li>Entre v&aacute;rios outros.&nbsp;</li>
                            </ul>

                            <p>Sabendo dessa versatilidade, n&atilde;o fica muito dif&iacute;cil entender que os procedimentos de reciclagem de lixo eletr&ocirc;nico n&atilde;o podem ser desenvolvidos da mesma maneira. Por isso, eles s&atilde;o divididos em reciclagem: t&eacute;rmica, qu&iacute;mica e mec&acirc;nica.&nbsp;</p>

                            <p>Tratando sobre a reciclagem t&eacute;rmica, &eacute; poss&iacute;vel descrever o procedimento como uma maneira de converter os metais em diferentes estados de pureza a partir da alta temperatura, processo conhecido como pirometalurgia.&nbsp;</p>

                            <p>Por outro lado, a reciclagem de lixo eletr&ocirc;nico qu&iacute;mica baseia-se na t&eacute;cnica da hidrometalurgia, que utiliza a lixivia&ccedil;&atilde;o com &aacute;gua-r&eacute;gia ou &aacute;cido-sulf&uacute;rico para obter e dividir fra&ccedil;&otilde;es pesadas (metais) de fra&ccedil;&otilde;es leves (pl&aacute;sticos, cer&acirc;micas, etc).&nbsp;</p>

                            <p>A &uacute;ltima, mas n&atilde;o menos importante, t&eacute;cnica de reciclagem &eacute; a mec&acirc;nica. Diferente das duas primeiras, ela &eacute; baseada completamente em processos &ldquo;bruscos&rdquo;, que envolvem atividades de britagem e moagem.&nbsp;</p>

                            <p>A partir delas, os metais s&atilde;o divididos em metais magn&eacute;ticos, categorizados em condutores e n&atilde;o condutores, e n&atilde;o magn&eacute;ticos. Ap&oacute;s isso, eles s&atilde;o enviados para descarte final ou reutiliza&ccedil;&atilde;o, dependendo de sua qualidade.&nbsp;</p>

                            <p>&Eacute; v&aacute;lido destacar que todas as pessoas podem produzir lixo eletr&ocirc;nico, fazendo com que a divis&atilde;o e descarte correto dos aparelhos quebrados ou obsoletos deva ser feita por todos de modo individual para um bem coletivo.&nbsp;</p>

                            <p>Est&aacute; em busca de uma empresa de reciclagem de lixo eletr&ocirc;nico para coletar todos os aparelhos que voc&ecirc; n&atilde;o jogou no lixo convencional e, consequentemente, contribuiu para preservar o planeta Terra? Ent&atilde;o, entre em contato com um dos parceiros do Solu&ccedil;&otilde;es Industriais!</p>


                            </article><span class="btn-leia">Leia Mais</span><span
                                class="btn-ocultar">Ocultar</span><span class=" leia"></span>
                        </div>
                        <div class="col-12 px-0">
                            <? include('inc/servicos/servicos-produtos-premium.php');?>
                        </div>
                        <? include('inc/servicos/servicos-produtos-fixos.php');?>
                        <? include('inc/servicos/servicos-imagens-fixos.php');?>
                        <? include('inc/servicos/servicos-produtos-random.php');?>
                        <hr />
                        <h2>Veja algumas referências de <?=$h1?> no youtube</h2>
                        <? include('inc/servicos/servicos-galeria-videos.php');?>
                    </section>
                    <? include('inc/servicos/servicos-coluna-lateral.php');?>
                    <h2>Galeria de Imagens Ilustrativas referente a <?=$h1?></h2>
                    <? include('inc/servicos/servicos-galeria-fixa.php');?> <span class="aviso">Estas imagens foram
                        obtidas de bancos de imagens públicas e disponível livremente na internet</span>
                    <? include('inc/regioes.php');?>
                </div>
    </main>
    </div><!-- .wrapper -->
    <? include('inc/footer.php');?>
    <!-- Tabs Regiões -->
    <script defer src="<?=$url?>js/organictabs.jquery.js"> </script>
    <script defer src="<?=$url?>inc/servicos/servicos-eventos.js"></script>
</body>

</html>