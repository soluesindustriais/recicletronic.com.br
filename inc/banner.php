        <div class="cd-hero">
    <ul class="cd-hero-slider autoplay">
        <li class="selected">
            <div class="cd-full-width">
                <h2>Onde descartar lixo eletrônico</h2>
                <p>Quem quer encontrar uma boa empresa onde descartar lixo eletrônico com referência no segmento, descobre o site da Recieletro. Na empresa é possível encontrar coleta de eletrônicos para reciclagem e coleta de peças de carro para reciclagem, disponibilizando tudo que há de mais atual para garantir a qualidade final para os clientes.</p>
                <a href="<?=$url?>onde-descartar-lixo-eletronico" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Empresa de reciclagem</h2>
                <p>Quem procura por uma empresa de reciclagem de eletrônicos precursora em tecnologia, encontra o site da Recieletro. Na empresa é possível achar coleta de eletrônicos e peças de carros para reciclagem, oferecendo sempre a melhor opção para os clientes.</p>
                <a href="<?=$url?>empresa-de-reciclagem" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Reciclagem de Eletrônicos</h2>
                <p>Se tratando de reciclagem de eletrônicos, dentre os vários ramos de atuação, é normalmente descrito como fundamental para aproveitar os detritos de um objeto e reutilizá-los como matéria-prima dentro do processo industrial.</p>
                <a href="<?=$url?>reciclagem-de-eletronicos" class="cd-btn">Saiba mais</a>
            </div>

        </li>
    </ul>
    <div class="cd-slider-nav">
        <nav>
            <span class="cd-marker item-1"></span>
            <ul>
                <li class="selected"><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
            </ul>
        </nav>
    </div>

</div>



        <!-- Hero End -->