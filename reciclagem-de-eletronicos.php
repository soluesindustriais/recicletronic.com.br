<? $h1 = "Reciclagem de Eletrônicos"; $title  = "Reciclagem de Eletrônicos"; $desc = "Receba orçamentos de Reciclagem de Eletrônicos, você vai encontrar na ferrementa Soluções Industriais, realize um orçamento já com mais de 200 fabrica"; $key  = "Reciclagem de Eletrônicos, Coleta de cobre"; include ('inc/head.php')?>

<body>
    <? include ('inc/header.php');?>
    <main><?=$caminhoservicos; include('inc/servicos/servicos-linkagem-interna.php');?><div
            class='container-fluid mb-2'>
            <? include('inc/servicos/servicos-buscas-relacionadas.php');?>
            <div class="container p-0">
                <div class="row no-gutters">
                    <section class="col-md-9 col-sm-12">
                        <div class="card card-body LeiaMais">
                            <h1 class="pb-2"><?=$h1?></h1>
                            <article>
                            <p>A reciclagem de eletr&ocirc;nicos &eacute; uma pr&aacute;tica que deve ser adotada de maneira individual para garantir a prote&ccedil;&atilde;o coletiva. Isso porque todas as pessoas podem produzir esse tipo de lixo, que &eacute; advindo do descarte de computadores, smartphones, televisores, aparelhos de som, geladeiras, fog&otilde;es, carregadores, baterias, fios, pilhas, etc.&nbsp;</p>

                            <p>Regulamentada em lei no Brasil, a pr&aacute;tica passou a ganhar ainda mais relev&acirc;ncia depois dos constantes desastres que o planeta vem enfrentando, como fortes ondas de calor na Europa, o derretimento das geleiras na Ant&aacute;rtida e na Groenl&acirc;ndia e as devastadoras queimadas na Amaz&ocirc;nia, no Pantanal e nas florestas da Austr&aacute;lia.&nbsp;</p>
                            
                            <p>Contudo, as medidas e a&ccedil;&otilde;es tomadas para introduzir pr&aacute;ticas sustent&aacute;veis no dia a dia da popula&ccedil;&atilde;o e at&eacute; mesmo nas pr&aacute;ticas industriais ainda n&atilde;o s&atilde;o suficientes. Segundo dados da Associa&ccedil;&atilde;o Internacional de Res&iacute;duos S&oacute;lidos (ISWA), o mundo produziu mais de 53 milh&otilde;es de toneladas de lixo tecnol&oacute;gico s&oacute; em 2019.&nbsp;</p>
                            
                            <p>Quando um dado envolvendo lixo vem em &ldquo;milh&otilde;es de toneladas&rdquo;, ele j&aacute; &eacute; assustador por si s&oacute;, mas essa ainda n&atilde;o &eacute; a pior parte! O F&oacute;rum Econ&ocirc;mico Mundial (WEF) de 2019 destacou que esse tipo de res&iacute;duo &eacute; o que mais cresce, mas a pr&aacute;tica de reciclagem de eletr&ocirc;nicos ainda est&aacute; estagnada nos 20%.&nbsp;</p>
                            
                            <p>Sabendo disso, &eacute; ainda mais importante ressaltar a import&acirc;ncia de cada um fazer a sua parte, pois se a produ&ccedil;&atilde;o de lixo continuar subindo e a reciclagem estagnada, o desenvolvimento de novas tecnologias pode ser comprometido e os danos causados ao meio ambiente irrepar&aacute;veis, o que poderia causar a pr&oacute;pria extin&ccedil;&atilde;o da humanidade.&nbsp;</p>
                            
                            <h2>COMO REALIZAR A RECICLAGEM DE ELETR&Ocirc;NICOS?&nbsp;</h2>
                            
                            <p>O primeiro passo para uma pessoa realizar a reciclagem de eletr&ocirc;nicos &eacute; sabendo que ela n&atilde;o pode - nem deve! - mistur&aacute;-lo com os lixos org&acirc;nicos ou de qualquer outra natureza, como o pl&aacute;stico, o papel, o vidro e o metal.&nbsp;</p>
                            
                            <p>Assim, todos os aparelhos e acess&oacute;rios eletr&ocirc;nicos que precisam ser descartados, sejam por estarem quebrados ou por terem se tornado obsoletos, devem ser separados com muito cuidado e aten&ccedil;&atilde;o.&nbsp;</p>
                            
                            <p>O principal motivo dessa cautela extra &eacute; devido ao fato que os aparelhos s&atilde;o fabricados com cobre, alum&iacute;nio, merc&uacute;rio, c&aacute;dmio, chumbo, etc, alguns com alta toxicidade, que quando na natureza pode causar a degrada&ccedil;&atilde;o do solo e a contamina&ccedil;&atilde;o de recursos naturais.</p>
                            
                            <p>Depois de realizar a separa&ccedil;&atilde;o, &eacute; o momento de pesquisar por uma empresa de reciclagem de eletr&ocirc;nicos, que s&atilde;o especializadas na coleta e destina&ccedil;&atilde;o de res&iacute;duos s&oacute;lidos relacionados a tecnologia. Normalmente, elas asseguram:&nbsp;</p>
                            
                            <ul>
                            	<li>Descaracteriza&ccedil;&atilde;o;</li>
                            	<li>Destrui&ccedil;&atilde;o;</li>
                            	<li>Reciclagem;</li>
                            	<li>Recompra;</li>
                            	<li>Recondicionamento;</li>
                            	<li>Revenda de aparelhos de inform&aacute;tica.&nbsp;</li>
                            </ul>
                            
                            <p>A companhia que realiza o servi&ccedil;o pode trabalhar de algumas maneiras distintas, como os pontos de coleta ou a retirada em resid&ecirc;ncias ou empresas. Em grandes centros urbanos, como S&atilde;o Paulo e Rio de Janeiro, &eacute; comum encontrar ambas as op&ccedil;&otilde;es, mas em cidades mais remotas, a iniciativa deve partir da uni&atilde;o da popula&ccedil;&atilde;o com a prefeitura.</p>
                            
                            <h2>OS PROCEDIMENTOS ADOTADOS PELAS EMPRESAS DO RAMO</h2>
                            
                            <p>Como j&aacute; citado, o lixo tecnol&oacute;gico envolve uma s&eacute;rie de equipamentos, aparelhos e acess&oacute;rios, que podem ser produzidos com diferentes mat&eacute;rias-primas. Por essa raz&atilde;o, &eacute; essencial que a reciclagem de eletr&ocirc;nicos seja desenvolvida de diferentes maneiras, como a t&eacute;rmica, a qu&iacute;mica e a mec&acirc;nica.&nbsp;</p>
                            
                            <p>No primeiro caso, os metais s&atilde;o convertidos em diferentes estados de pureza em um processo que envolve temperaturas muito elevadas. Enquanto isso, o segundo se destaca por utilizar a lixivia&ccedil;&atilde;o com &aacute;gua-r&eacute;gia ou &aacute;cido-sulf&uacute;rico para garantir a separa&ccedil;&atilde;o dos metais dos outros materiais (pl&aacute;stico, cer&acirc;mica, etc).&nbsp;</p>
                            
                            <p>J&aacute; a reciclagem mec&acirc;nica de eletr&ocirc;nicos &eacute; desenvolvida por meio de processos de britagem e moagem, que divide os metais magn&eacute;ticos dos n&atilde;o magn&eacute;ticos para s&oacute; ent&atilde;o realizar o descarte ou reutiliza&ccedil;&atilde;o. Independentemente do processo desenvolvido, ele deve ser feito por profissionais qualificados.&nbsp;</p>
                            
                            <p>Em conclus&atilde;o, &eacute; fundamental ressaltar que a reciclagem de eletr&ocirc;nicos n&atilde;o beneficia apenas o meio ambiente. Muitas das mat&eacute;rias-primas utilizadas s&atilde;o t&oacute;xicas para o ser humano, que pode desenvolver doen&ccedil;as graves e at&eacute; fatais quando em contato com eles depois de deteriorados.</p>
                            
                            <p>Clientes que pesquisam por uma empresa de reciclagem de eletr&ocirc;nicos podem entrar em contato com um dos parceiros do Solu&ccedil;&otilde;es Industriais. Considerado a maior plataforma B2B da Am&eacute;rica Latina, o portal re&uacute;ne as melhores companhias do segmento.</p>

                            </article><span class="btn-leia">Leia Mais</span><span
                                class="btn-ocultar">Ocultar</span><span class=" leia"></span>
                        </div>
                        <div class="col-12 px-0">
                            <? include('inc/servicos/servicos-produtos-premium.php');?>
                        </div>
                        <? include('inc/servicos/servicos-produtos-fixos.php');?>
                        <? include('inc/servicos/servicos-imagens-fixos.php');?>
                        <? include('inc/servicos/servicos-produtos-random.php');?>
                        <hr />
                        <h2>Veja algumas referências de <?=$h1?> no youtube</h2>
                        <? include('inc/servicos/servicos-galeria-videos.php');?>
                    </section>
                    <? include('inc/servicos/servicos-coluna-lateral.php');?>
                    <h2>Galeria de Imagens Ilustrativas referente a <?=$h1?></h2>
                    <? include('inc/servicos/servicos-galeria-fixa.php');?> <span class="aviso">Estas imagens foram
                        obtidas de bancos de imagens públicas e disponível livremente na internet</span>
                    <? include('inc/regioes.php');?>
                </div>
    </main>
    </div><!-- .wrapper -->
    <? include('inc/footer.php');?>
    <!-- Tabs Regiões -->
    <script defer src="<?=$url?>js/organictabs.jquery.js"> </script>
    <script defer src="<?=$url?>inc/servicos/servicos-eventos.js"></script>
</body>

</html>